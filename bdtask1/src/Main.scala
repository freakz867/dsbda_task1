import org.apache.hadoop._
import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce.lib.input._
import org.apache.hadoop.mapreduce.lib.output._
import org.apache.hadoop.util.GenericOptionsParser

/**
 * Main application object.
 */
object Main extends App {
  val name = "Impression counter"

  val configuration = new conf.Configuration
  val optionParser = new GenericOptionsParser(configuration, args)
  val workArgs = optionParser.getRemainingArgs

  /* Create and configure the job. */
  val job = mapreduce.Job.getInstance(configuration, name)

  job.setJarByClass(classOf[ImpressionMapper])
  job.setMapperClass(classOf[ImpressionMapper])
  job.setCombinerClass(classOf[ImpressionCombiner])
  job.setReducerClass(classOf[ImpressionReducer])
  /* Set 2 reduce tasks */
  job.setNumReduceTasks(2)

  job.setMapOutputKeyClass(classOf[Text])
  job.setMapOutputValueClass(classOf[CompositeWritable])
  job.setOutputKeyClass(classOf[Text])
  job.setOutputValueClass(classOf[IntWritable])

  job.setInputFormatClass(classOf[TextInputFormat])
  job.setOutputFormatClass(classOf[SequenceFileOutputFormat[Text, IntWritable]])

  /* Add cities and regions files to local cache */
  job.addCacheFile(new fs.Path("cache/city.en.txt").toUri)
  job.addCacheFile(new fs.Path("cache/region.en.txt").toUri)

  FileInputFormat.setInputPaths(job, new fs.Path(workArgs(0)))
  FileOutputFormat.setOutputPath(job, new fs.Path(workArgs(1)))

  /* Enable snappy encoding */
  FileOutputFormat.setCompressOutput(job, true);
  FileOutputFormat.setOutputCompressorClass(job, classOf[compress.SnappyCodec]);
  SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

  sys.exit(if (job.waitForCompletion(true)) 0 else 1)
}
