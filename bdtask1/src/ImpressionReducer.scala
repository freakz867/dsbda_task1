import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce._
import org.apache.log4j.Logger

/**
 * Provides Reducer implementation.
 * Sums all flagged records, which was flagged in Combiner.
 */
class ImpressionReducer extends ImpressionReducer.Base {
  import ImpressionReducer._

  val logger = Logger.getLogger(classOf[ImpressionReducer])

  override def reduce(key: Text, values: java.lang.Iterable[CompositeWritable], context: Base#Context): Unit = {
    import scala.collection.JavaConverters._

    var total_count = 0
    values.forEach { value =>
      total_count += value.getFirst
    }
    res.set(total_count)
    context.write(key, res)
  }
}

/**
 * Static fields and type definitions for ImpressionReducer class.
 */
object ImpressionReducer {
  type Base = Reducer[Text, CompositeWritable, Text, IntWritable]

  /**
   * Reusable Writeable for int result.
   */
  val res = new IntWritable(0)
}
