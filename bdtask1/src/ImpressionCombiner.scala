import org.apache.hadoop.io._
import org.apache.hadoop.mapreduce._
import org.apache.log4j.Logger

/**
 * Provides Combiner implementation.
 * Make record flagged if bidding price is over 255.
 */
class ImpressionCombiner extends ImpressionCombiner.Base {
  import ImpressionCombiner._

  val logger = Logger.getLogger(classOf[ImpressionCombiner])

  override def reduce(key: Text, values: java.lang.Iterable[CompositeWritable], context: Base#Context): Unit = {
    import scala.collection.JavaConverters._

    var flagged = 0
    var bidding_price = 0
    var temp = 0
    values.forEach { value =>
      bidding_price = value.getSecond
      temp = if (bidding_price >= 255) 1 else 0
      flagged += temp
    }
    composite.set(flagged, bidding_price)
    context.write(key, composite)
  }
}

/**
 * Static fields and type definitions for ImpressionCombiner class.
 */
object ImpressionCombiner {
  type Base = Reducer[Text, CompositeWritable, Text, CompositeWritable]

  /**
   * Reusable Writeable for composite values.
   */
  val composite = new CompositeWritable
}
