import org.apache.hadoop.io._
import org.apache.hadoop.fs._
import org.apache.hadoop.mapreduce._
import org.apache.log4j.Logger

import scala.annotation.tailrec

/**
 * Provides Mapper implementation.
 * Extracts city and bidding price from line.
 */
class ImpressionMapper extends ImpressionMapper.Base {
  import ImpressionMapper._

  val logger = Logger.getLogger(classOf[ImpressionMapper])

  override def setup(context: Mapper[Any,Text,Text,CompositeWritable]#Context) { 
    var cacheFilesLocal = context.getCacheFiles();
    var fs = FileSystem.get(context.getConfiguration());
    var lines = scala.io.Source.fromInputStream(fs.open(new Path(cacheFilesLocal.head))).getLines
    lines.foreach{ line =>
       var temp = line.split("\\s+")
      cities += (temp(0).toInt -> temp(1).toString)
    }
  }

  override def map(key: Any, value: Text, context: Base#Context): Unit = {
    /* 
     * 1. Extract city ID and bidding price.
     */

    logger.info(value.toString)

    var arr = value.toString.split('\t')
    val region = arr(6).toString
    val city = arr(7).toInt
    val bid = arr(19).toInt
    text.set(cities(city))
    composite.set(0, bid)
    context.write(text, composite)
  }
}

/**
 * Static fields and type definitions for ImpressionMapper class.
 */
object ImpressionMapper {
  type Base = Mapper[Any, Text, Text, CompositeWritable]

  /**
   * Reusable Writable for texts.
   */
  val text = new Text
  /**
   * Reusable Writeable for composite values.
   */
  val composite = new CompositeWritable
  var cities = collection.mutable.Map[Int, String]().withDefaultValue("Noname")
  var regions = collection.mutable.Map[Int, String]().withDefaultValue("Noname")

}
