# Task 1

## Условия задачи

* Входные данные — dataset http://data.computational-advertising.org
* Выходные данные — SequenceFile с snappy-сжатием

### Дополнительные условия

* Custom Type is defined and used
* Writable entities are reused

### Содержимое отчёта

* ZIP-ed src folder with your implementation
* Screenshot of successfully executed tests
* Screenshot of successfully uploaded file into HDFS
* Screenshots of successfully executed job and result
* Number of Reducers more than 1. Screenshots of successfully executed job with several
reducers and result
* Screenshot of reading compressed content
* Quick build and deploy manual (commands, OS requirements etc)

## Развёртывание системы

### Требования

* Linux-based операционная система
* Установленные пакеты `docker` и `docker-compose`
* Установленные пакеты `jdk8` и `sbt`

### Конфигурация и запуск кластера

Для создания Hadoop-кластера использован готовый образ Docker из репозитория `kiwenlau/hadoop`.
Данный образ модифицирован для работы с Java 8 и snappy-сжатием, для сборки образа необходимо
в директории `environment` выполнить команду `./build.sh`.

Конфигурация кластера производится командой `./configure.sh`, которая принимает на вход число
data node, например, `./configure.sh 2`. После выполнения данной команды в директории `environment`
будет создан файл `docker-compose.yml` с описанием конфигурации системы.

Для запуска системы необходимо выполнить команду `docker-compose up` с правами суперпользователя.

Остановка система производится аналогичной командой `docker-compose down`.

### Сборка проекта
Основным каталогом является task1/bdtask1
Для сборки проекта необходимо ввести команду `sbt assembly` в директории `task1/bdtask1`.
В ходе сборки будут загружены необходимые зависимости, выполнены unit-тесты, а также собран
`jar`-файл для исполнения в Hadoop.

* Результат unit-тестов
screenshots/tests*.png

### Запуск задачи

Для создания входных данных и запуска задачи используется скрипт `./run.sh`
в директории `environment`. При выполнении данного скрипта файлы dataset и cache будут загружены в HDFS.
Во время выполнения скрипта будет также загружен и исполнен `jar`-файл, который должен быть
предварительно собран по инструкции выше.

* Загруженные входные данные.
screenshots/input*.png

* Ход выполнения задачи и информация о потреблении ресурсов.
screenshots/job*.png

* Отдельно число редюсеров
screenshots/job3.png

После выполнения задачи скрипт выведет на экран выходные данных, предварительно произведя
их распаковку и преобразование в текст.

* Выходные данные в сжатом виде.
screenshots/output_encoded*.png

* Выходные данные в текстовом виде.
screenshots/output_text*.png

