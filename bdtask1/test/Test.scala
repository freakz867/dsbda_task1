import org.apache.hadoop.io._
import org.apache.hadoop.mrunit.mapreduce._

import org.scalatest.FlatSpec

import scala.collection.JavaConverters._

class Test extends FlatSpec {
  val mapper = new ImpressionMapper
  val combiner = new ImpressionCombiner
  val reducer = new ImpressionReducer

  def mapDriver = MapDriver.newMapDriver(mapper)
  def combineDriver = ReduceDriver.newReduceDriver(combiner)
  def reduceDriver = ReduceDriver.newReduceDriver(reducer)

  val testStrings = io.Source.fromFile("test.txt").getLines.toArray

  "The ImpressionMapper" should "return CompositeWritble with parsed bidding price" in {
    val suiteMapDriver = mapDriver
    suiteMapDriver.withInput(new Text(""), new Text(testStrings(0)))
    suiteMapDriver.withOutput(new Text("Noname"), new CompositeWritable(0, 277))
    suiteMapDriver.runTest()
  }

  "The ImpressionMapper" should "return CompositeWritble with parsed bidding price and Noname in city" in {
    val suiteMapDriver = mapDriver
    suiteMapDriver.withInput(new Text(""), new Text(testStrings(1)))
    suiteMapDriver.withOutput(new Text("Noname"), new CompositeWritable(0, 294))
    suiteMapDriver.runTest()
  }

  "The ImpressionCombiner" should "return quantity of bids over 255: 0" in {
    val suiteCombineDriver = combineDriver
    suiteCombineDriver.withInput(
      new Text("guanzhou"),
      List(new CompositeWritable(0, 248), new CompositeWritable(0, 146)).asJava
    )
    suiteCombineDriver.withOutput(new Text("guanzhou"), new CompositeWritable(0, 146))
    suiteCombineDriver.runTest()
  }

  "The ImpressionCombiner" should "return quantity of bids over 255: 1" in {
    val suiteCombineDriver = combineDriver
    suiteCombineDriver.withInput(
      new Text("guanzhou"),
      List(new CompositeWritable(0, 248), new CompositeWritable(0, 262)).asJava
    )
    suiteCombineDriver.withOutput(new Text("guanzhou"), new CompositeWritable(1, 262))
    suiteCombineDriver.runTest()
  }

  "The ImpressionReducer" should "return sum of records, where quantity of bids over 255" in {
    val suiteReduceDriver = reduceDriver
    suiteReduceDriver.withInput(
      new Text("guanzhou"),
      List(new CompositeWritable(0, 245), new CompositeWritable(1, 265)).asJava
    )
    suiteReduceDriver.withOutput(new Text("guanzhou"), new IntWritable(1))
    suiteReduceDriver.runTest()
  }
}
