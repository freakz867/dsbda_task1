#!/bin/bash

read -r -d '' code << 'EOF'

echo "==> Starting task..."
hadoop fs -rm -r -f input output_dir cache
hadoop fs -mkdir -p input
hadoop fs -put input/* input
hadoop fs -ls input
hadoop fs -put cache/* cache
hadoop fs -ls cache
hadoop jar /opt/mapreduce*.jar input output
echo "==> Pulling result..."
hadoop fs -cat output/part-r-00000
echo
hadoop fs -text output/part-r-00000

EOF

sudo docker cp ../mapreduce/target/scala-2.12/mapreduce*.jar hadoop-master:/opt/ || exit $?
sudo docker exec -ti hadoop-master /bin/bash -c "$code" bash "$@" || exit $?
